package hu.featka.erlang.wifi.manager;

import java.lang.reflect.Method;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;
import android.net.wifi.*;
import com.ericsson.otp.erlang.*;

public class WifiNode extends Service {

	private OtpNode self;
	private OtpMbox mbox;
	private int startUpStatus;

	public WifiNode() {
		super();
	}

	@Override
	public IBinder onBind(Intent arg0) {
		return null;
	}

	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		Log.d("ErlangWifiManager", "starting");
		new Thread(new Runnable() {
			public void run() {
				WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
				int ip = wifi.getConnectionInfo().getIpAddress();

				startUpStatus = makeNode(intToIp(ip));
				if (startUpStatus == 0) {
					serverProcess();
				}
			}
		}).start();
		return START_STICKY;
	}

	@Override
	public void onDestroy() {
		WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		wifi.setWifiEnabled(false);
		Method[] wmMethods = wifi.getClass().getDeclaredMethods();

		for (Method method : wmMethods) {
			if (method.getName().equals("setWifiApEnabled")) {
				try {
					method.invoke(wifi, null, false);
					Log.d("ErlangWifiManager", "ap destroyed");
				} catch (Exception e) {
					Log.e("ErlangWifiManager", "error", e);
				}
			}
		}

		Log.d("ErlangWifiManager", "shutting down");
		self = null;
		mbox = null;
	}

	public String intToIp(int i) {

		return (i & 0xFF) + "." + ((i >> 8) & 0xFF) + "." + ((i >> 16) & 0xFF)
				+ "." + ((i >> 24) & 0xFF);
	}

	// From this part on these functions run the node.
	private int makeNode(String ip) {
		int status = 0;
		try {
			String client = "fclient@" + ip;
			Log.d("ErlangWifiManager", client);
			Log.d("ErlangWifiManager", "creating node");
			self = new OtpNode("wifinode", "mycookie");
			if (self.ping(client, 2000)) {
				Log.d("ErlangWifiManager", "connected");
			} else {
				Log.d("ErlangWifiManager", "not connected");
			}
			Log.d("ErlangWifiManager", "node done, creating mbox");
			mbox = self.createMbox("wifiserver");
		} catch (Exception e) {
			Log.e("ErlangWifiManager", "error", e);
			status = 1;
		}
		return status;
	}

	private void serverProcess() {
		Log.d("ErlangWifiManager", "starting serverProcess");
		OtpErlangObject o;
		while (true) {
			try {
				Log.d("ErlangWifiManager", "receiving something");
				o = mbox.receive();
				Log.d("ErlangWifiManager", "received something");
				Log.i("ErlangWifiManager", o.toString());
				if (o instanceof OtpErlangTuple) {
					Log.d("ErlangWifiManager", "received something useful");
					wifiMaker((OtpErlangTuple) o);
				} else {
					Log.d("ErlangWifiManager", "received something else");
					Log.i("ErlangWifiManager", o.toString());
				}
			} catch (Exception e) {
				Log.e("ErlangWifiManager", "error", e);
			}
		}
	}

	// decides what should happen with the received tupple
	private void wifiMaker(OtpErlangTuple what) {
		OtpErlangAtom ap = new OtpErlangAtom("make_ap");
		OtpErlangAtom conn = new OtpErlangAtom("connect_to");

		if (what.elementAt(0).equals(ap)) {
			if (what.elementAt(1) instanceof OtpErlangTuple) {
				OtpErlangTuple temp = (OtpErlangTuple) what.elementAt(1);
				String ssid = ((OtpErlangString) temp.elementAt(0)).toString()
						.substring(
								1,
								((OtpErlangString) temp.elementAt(0))
										.toString().length() - 1);
				String passwd = ((OtpErlangString) temp.elementAt(1))
						.toString().substring(
								1,
								((OtpErlangString) temp.elementAt(1))
										.toString().length() - 1);
				makeAP(ssid, passwd);
			}

		} else if (what.elementAt(0).equals(conn)) {
			OtpErlangTuple temp = (OtpErlangTuple) what.elementAt(1);
			String ssid = ((OtpErlangString) temp.elementAt(0)).toString()
					.substring(
							1,
							((OtpErlangString) temp.elementAt(0)).toString()
									.length() - 1);
			String passwd = ((OtpErlangString) temp.elementAt(1)).toString()
					.substring(
							1,
							((OtpErlangString) temp.elementAt(1)).toString()
									.length() - 1);
			connect(ssid, passwd);
		}
	}

	// making an access point
	private void makeAP(String ssid, String passwd) {
		WifiConfiguration config = new WifiConfiguration();
		config.SSID = ssid;
		config.preSharedKey = passwd;
		config.allowedKeyManagement.set(WifiConfiguration.KeyMgmt.WPA_PSK);
		config.allowedAuthAlgorithms.set(WifiConfiguration.AuthAlgorithm.OPEN);
		WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		wifi.setWifiEnabled(false);
		Method[] wmMethods = wifi.getClass().getDeclaredMethods();

		for (Method method : wmMethods) {
			if (method.getName().equals("setWifiApEnabled")) {
				try {
					method.invoke(wifi, config, true);
					Log.d("ErlangWifiManager", "ap created");
				} catch (Exception e) {
					Log.e("ErlangWifiManager", "error at ap creation", e);
				}
			}
		}
	}

	// connecting to the AP
	private void connect(String ssid, String passwd) {
		WifiConfiguration config = new WifiConfiguration();
		config.SSID = ssid;
		config.preSharedKey = passwd;

		WifiManager wifi = (WifiManager) getSystemService(Context.WIFI_SERVICE);
		int netID = wifi.addNetwork(config);
		wifi.saveConfiguration();
		wifi.disconnect();
		wifi.enableNetwork(netID, true);
		wifi.reconnect();
		Log.d("ErlangWifiManager", "wifi client connection");
	}
}
