package hu.featka.erlang.wifi.manager;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;

public class ShutdownNode extends Service {

	public ShutdownNode() {
		super();// TODO Auto-generated constructor stub
	}

	@Override
	public IBinder onBind(Intent intent) {
		// TODO Auto-generated method stub
		return null;
	}
	public int onStartCommand(Intent intent, int flags, int startId) {
		Intent service = new Intent();
		service.setClassName("hu.featka.erlang.wifi.manager", "hu.featka.erlang.wifi.manager.WifiNode");
		stopService(service);
		stopSelf();
		return START_NOT_STICKY;
	}

}
