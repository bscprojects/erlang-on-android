%%%-------------------------------------------------------------------
%%% @author  <featka@FEATKA-NOTEBOOK>
%%% @copyright (C) 2013, 
%%% @doc
%%%
%%% @end
%%% Created : 13 Mar 2013 by  <featka@FEATKA-NOTEBOOK>
%%%-------------------------------------------------------------------

-module(server).

-export([start/0, stop/0]).


start() ->
    register(csrv, spawn(fun() -> init() end)).

init() ->
    ets:new(users, [set, public, named_table]),
    loop().

loop() ->
    receive
	stop ->
	    ets:delete(users),
	    ok;

	{register, {Name,Lat,Long,Node}} ->
	    io:format("register received~n"),
	    Time = calendar:universal_time(),
	    NotExists = insert_table(Name,Lat,Long,Node,Time),
	    case NotExists of
	    	true -> 
		    {cclient,Node} ! register_ok,
		    io:format("register_ok sent~n");
	    	false ->
		    {cclient,Node} ! {register_error, "Already registered name"},
		    io:format("register_error sent ~n")
	    end,
	    loop();

	{update, {Name,Lat,Long}} ->
	    io:format("update received~n"),
	    update_table(Name,Lat,Long),
	    loop();
	{unregister, Name} ->
	    io:format("logout received~n"),
	    ets:delete(users,Name),
	    loop();
	{connect, {Name,NameToConnect}} ->
	    io:format("connect received~n"),
	    Data = ets:lookup(users, NameToConnect),
	    [{NameFrom, LatFrom, LongFrom, NodeFrom, TimeFrom}] = ets:lookup(users,Name),
	    {Status,Msg} = compute({NameFrom,LatFrom,LongFrom,NodeFrom,TimeFrom},Data),
	    case Status of
		ok -> 
		    io:format("connecting ok ~n"),
		    {cclient,NodeFrom} ! {make_ap,{"ErlangAP","qwert123"}},
		    {cclient,Msg} ! {connect_to, {"ErlangAP","qwer123",NodeFrom}};
		toofar -> 
		    io:format("toofar~n"),
		    {cclient,NodeFrom} ! {toofar,Msg};
		missing -> 
		    io:format("missing~n"),
		    {cclient,NodeFrom} ! missing;
		old_data -> 
		    io:format("old_data~n"),
		    {cclient,NodeFrom} ! force_update,
		    {cclient,Msg} ! force_update,
		    {cclient,NodeFrom} ! {error, "Data were too old, try again after a few seconds."};
		_ -> ok
	    end,
	    loop();
	
	_ ->
	    loop()
		end.

compute(ConnectFrom,ConnectTo)->
    case ConnectTo of
	[] -> {missing,[]};
	[Data] -> 
	    makeMsg(ConnectFrom,Data);
	_ -> error
    end.

update_table(Name, Lat, Long) ->
    Time = calendar:universal_time(),
    ets:update_element(users, Name, [{2,Lat}, {3,Long}, {5,Time}]).

insert_table(Name, Lat, Long, Node, Time) ->
    Exists = ets:member(users,Name),
    case Exists of
	false ->
	    ets:insert(users,{Name,Lat,Long,Node,Time});
	true ->
	    false
		end.
stop() ->
    csrv ! stop.

makeMsg({_NameFrom,LatFrom,LongFrom,_NodeFrom,TimeFrom},
	{_NameTo,LatTo,LongTo,NodeTo,TimeTo}) ->
    Dist = abs(calculate_distance(LatFrom,LongFrom,LatTo,LongTo)),
    TimeDif = time_dif(TimeFrom,TimeTo),
    NowDif = time_dif(TimeFrom,calendar:universal_time()),
    if
	(Dist > 50)  ->
	    {toofar, {50,Dist}};
	(TimeDif > 2) or (NowDif > 2)->
	    {old_data, NodeTo};
	true ->
	    {ok, NodeTo}
    end.

time_dif(TimeFrom,TimeTo) ->
    SecondsFrom = calendar:datetime_to_gregorian_seconds(TimeFrom),
    SecondsTo = calendar:datetime_to_gregorian_seconds(TimeTo),
    abs((SecondsFrom - SecondsTo) / 60).
    
calculate_distance(LatFrom,LongFrom,LatTo,LongTo) ->
    Lat = (LatFrom - LatTo) * math:pi() / 180,
    Long = (LongFrom - LongTo) * math:pi() / 180,
    A = math:pow(math:sin(Lat / 2), 2) + 
	math:cos(LatTo * math:pi() / 180) * 
	math:cos(LatFrom * math:pi() / 180) * 
	math:pow(math:sin(Long / 2), 2),
    math:atan2(math:sqrt(A), math:sqrt(1-A)) * 12.734.

    
