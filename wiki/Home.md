# About the project #

This project is about making two Android phones connect over WiFi, and make some communication between then with Erlang nodes. The connection is based upon the distance between the two mobile phones. 

The project consists of two individual parts: a server application wich coordinates the connection between the phones, and the client applications on the phones. The clients only communicate with the server to get the connection information.

# The Clients #

On the client side, we need to access the mobile's location data through Android API. For this we have to create in C++ a little code to get the GPS location info from the sensors, then get it into the Erlang VM. 
We have to make another C++ "library" for managing the connection between the phones with the given parameters.

After the two phones are connected, the communication between them is done between two Erlang nodes over WiFi.

# The Server #

On the server side, we have to get the sensor data from the phones, make calculations of the distance between them, and if they're in range of each other, make them connect.

First the clients need to "register", otherwise the server doesn't know that there is someone trying to connect to somebody. For this, the client needs to send a unique name, the GPS sensor data, and the cookie set in the Erlang VM to the server. Receiving this data would be achieved through the base communication method between Erlang nodes. 

Depending on the distance between the phones, the server either sends the connect command with the needed parameters, or sends an error message telling to get closer to each other.

*More about the server [here](server)*