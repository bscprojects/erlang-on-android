-module(iris_test).

-export([test/0]).

test() ->
    Port = open_port({spawn, "./iris-linux-amd64 -dev"}, [stream, {line, 100}, stderr_to_stdout]),
    {os_pid, OsPid} = erlang:port_info(Port, os_pid),
    KillCommand = string:concat(checkEnv(), "~p"),
    timer:sleep(10000),
    os:cmd(io_lib:format(KillCommand, [OsPid])),
    timer:sleep(2000),
    c:flush().

checkEnv() ->
    case os:type() of
	{unix,linux} ->
	    "kill ";
	
	_ -> 
	    "taskkill /PID " 
    end.

