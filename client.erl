-module(client).

-export([main/0]).

-define(SRV, {csrv,'server@192.168.229.1'}).

layout() ->
    %% This layout was greated using Eclipse
"<LinearLayout xmlns:android=\"http://schemas.android.com/apk/res/android\"
    android:layout_width=\"match_parent\"
    android:layout_height=\"match_parent\"
    android:orientation=\"vertical\" 
    android:background=\"#ffffff\" >

    <LinearLayout
        android:layout_width=\"match_parent\"
        android:layout_height=\"wrap_content\" >

        <EditText
            android:id=\"@+id/nickname\"
            android:layout_width=\"match_parent\"
            android:layout_height=\"wrap_content\"
            android:layout_weight=\"1\"
            android:ems=\"10\"
            android:hint=\"Nickname\" />

        <Button
            android:id=\"@+id/registerButton\"
            android:layout_width=\"wrap_content\"
            android:layout_height=\"wrap_content\"
            android:text=\"Register\" 
            android:clickable=\"true\" />
        <Button
            android:id=\"@+id/unregisterButton\"
            android:layout_width=\"wrap_content\"
            android:layout_height=\"wrap_content\"
            android:text=\"Unregister\" 
            android:clickable=\"true\" />
        <Button
            android:id=\"@+id/updateButton\"
            android:layout_width=\"wrap_content\"
            android:layout_height=\"wrap_content\"
            android:text=\"Update\" 
            android:clickable=\"true\" />

    </LinearLayout>
    <LinearLayout
        android:layout_width=\"match_parent\"
        android:layout_height=\"wrap_content\" >
        <EditText
            android:id=\"@+id/connectTo\"
            android:enabled=\"true\"
            android:focusable=\"true\"
            android:layout_width=\"match_parent\"
            android:layout_height=\"wrap_content\"
            android:layout_weight=\"1\"
            android:ems=\"10\"
            android:hint=\"Connect to\" />
        <Button
            android:id=\"@+id/connectButton\"
            android:layout_width=\"wrap_content\"
            android:layout_height=\"wrap_content\"
            android:layout_gravity=\"center_horizontal\"
            android:text=\"Connect\"
            android:clickable=\"true\" />
        <Button
            android:id=\"@+id/disconnectButton\"
            android:layout_width=\"wrap_content\"
            android:layout_height=\"wrap_content\"
            android:layout_gravity=\"center_horizontal\"
            android:text=\"Disconnect\"
            android:clickable=\"true\" />
    </LinearLayout>

    <LinearLayout
        android:layout_width=\"match_parent\"
        android:layout_height=\"wrap_content\" >

        <EditText
            android:id=\"@+id/message\"
            android:enabled=\"true\"
            android:focusable=\"true\"
            android:layout_width=\"match_parent\"
            android:layout_height=\"wrap_content\"
            android:layout_weight=\"1\"
            android:ems=\"10\"
            android:hint=\"Message\" />

        <Button
            android:id=\"@+id/sendButton\"
            android:layout_width=\"wrap_content\"
            android:layout_height=\"wrap_content\"
            android:text=\"Send\"
            android:clickable=\"true\" />
    </LinearLayout>

    <TextView
        android:id=\"@+id/received\"

        android:layout_width=\"wrap_content\"
        android:layout_height=\"wrap_content\"
        android:layout_gravity=\"center_horizontal\"
        android:layout_marginTop=\"20dp\"
        android:textAppearance=\"?android:attr/textAppearanceMedium\" />

    <RelativeLayout
        android:layout_width=\"match_parent\"
        android:layout_height=\"match_parent\" >

        <TextView
            android:id=\"@+id/statusBar\"
            android:layout_width=\"wrap_content\"
            android:layout_height=\"wrap_content\"
            android:layout_alignParentBottom=\"true\"
            android:layout_alignParentLeft=\"true\"
            android:layout_alignParentRight=\"true\" />

    </RelativeLayout>

</LinearLayout>
".



main() ->
    android:startLocating(),
    register(cclient, spawn(fun() -> cinit() end)),
    android:fullShow(layout()),
    os:cmd("su -c am startservice hu.featka.erlang.wifi.manager/.WifiNode"),
    eventLoop().

eventLoop() ->
    Event = android:eventWait(),
    case Event of
	{struct, Props} ->
	    case proplists:get_value("name", Props) of
		"click" ->
		    {struct, Data} = proplists:get_value("data", Props),
		    ID = proplists:get_value("id", Data),
		    case ID of
			"registerButton" ->
			     {_Proc, Node} = ?SRV,
			    case net_adm:ping(Node) of
				pong ->
				    android:makeToast("registering"),
				    {Lat,Long} = get_location(android:readLocation()),
				    Name = get_property("nickname","text"),
				    ?SRV ! {register, {Name,Lat,Long,node()}};
				pang ->
				    android:fullSetProperty("statusBar", "text", "The server is unreachable.");
				_ -> ok
			    end;
			    
			"unregisterButton" ->
			    unregister();
			"updateButton" ->
			    update();
			"connectButton" ->
			    connect();
			"disconnectButton" ->
			    stop();
			"sendButton" ->
			    send();
			"asdasdasda" ->
			    Res = lists:flatten(io_lib:format("~w", [node()])),
			    android:fullSetProperty("statusBar", "text", Res);
			"apButtona" ->
			    Status = net_adm:ping('wifinode@localhost'),
			    {wifiserver,'wifinode@localhost'} ! {make_ap, {"tesztAP","12345678"}},
			    android:fullSetProperty("resultView", "text", erlang:atom_to_list(Status));
			_ ->
			    ok
		    end;
		"answered" -> 
		    {struct, Data} = proplists:get_value("data", Props),
		    case Props of
			"register_ok" ->
			    android:makeToast("register_ok");
			_ -> android:makeToast("hahahahah")
		    end;
		

		"key" ->
		    {struct, Data} = proplists:get_value("data", Props),
		    Key = proplists:get_value("key", Data),
		    case Key of
			"4" ->
			    %% key 4 is the back key; pressing this will
			    %% stop the application
			    stop();
			
			_ ->
			    ok
		    end;
		_ ->
		    ok
	    end;
	_ ->
	    ok
    end,
    eventLoop().
		
%% test() ->
%%     IP = get_property("serverText", "text"),
%%     {ok, Socket} = gen_tcp:connect(IP, 2000, [binary,{packet,4}]),
%%     case catch list_to_integer(get_property("inputText", "text")) of
%% 	{'EXIT', _} ->
%% 	    %% not an integer yet
%% 	    0;
%% 	Value ->
%% 	    gen_tcp:send(Socket, term_to_binary({fac,Value})),
%% 	    Result = receive
%% 		      {tcp, Socket, Bin} ->
%% 			  binary_to_term(Bin)
%% 		  end,
%% 	    gen_tcp:close(Socket),
%% 	    Result
%%     end.

send() ->
    IP = get_property("serverText", "text"),
    {ok, Socket} = gen_tcp:connect(IP, 2000, [binary,{packet,4}]),
    gen_tcp:send(Socket, term_to_binary({location,get_location(android:readLocation())})),
    gen_tcp:close(Socket).
    

get_location({struct, Data}) ->
    GPS = proplists:get_value("gps",Data),
    case GPS of
	{struct, GPS_data} ->
	    {proplists:get_value("latitude",GPS_data) , proplists:get_value("longitude",GPS_data)};
	_ ->
	    Network_loc = proplists:get_value("network",Data),
	    case Network_loc of
		{struct, Nw_data} ->
		    {proplists:get_value("latitude",Nw_data) , proplists:get_value("longitude",Nw_data)}
	    end
    end.

get_property(ID, Prop) ->
    case android:fullQueryDetail(ID) of
	{struct, P} ->
	    proplists:get_value(Prop, P);
	_ ->
	    undefined
    end.

stop() ->
    cclient ! stop,
    android:stopLocating(),
    os:cmd("su -c am startservice hu.featka.erlang.wifi.manager/.ShutdownNode"),
    init:stop().

%% register() ->
   

unregister() ->
    Name= get_property("nickname","text"),
    ?SRV ! {unregister, Name}.

update() ->
    Name = get_property("nickname","text"),
    {Lat,Long} = get_location(android:readLocation()),
    ?SRV ! {update, {Name,Lat,Long}}.

connect() ->
    Name = get_property("nickname","text"),
    NameToConnect = get_property("connectTo", "text"),
    ?SRV ! {connect, {Name, NameToConnect}}.

    

    

%% a kommunikacios resz implementacioja
%% itt kezdodik

cinit() ->
    cloop().

cloop() ->
    receive
	    
	register_ok ->
	    android:makeToast("registered"),
	    android:fullSetProperty("statusBar", "text", "Client registered on the server."),
	    %% android:fullSetProperty("unregisterButton", "clickable", "true"),
	    %% android:fullSetProperty("registerButton", "clickable", "false"),
	    %% android:fullSetProperty("nickname", "enabled", "false"),
	    %% android:fullSetProperty("nickname", "focusable", "false"),
	    %% android:fullSetProperty("connectTo", "enabled", "true"),
	    %% android:fullSetProperty("connectTo", "focusable", "false"),
	    %% android:fullSetProperty("updateButton", "clickable", "true"),
	    cloop();
	{register_error, Error} ->
	    android:fullSetProperty("statusBar", "text", Error),
	    cloop();

	unregister_ok ->
	    android:fullSetProperty("statusBar", "text", "Client unregistered."),
	    %% android:fullSetProperty("unregisterButton", "clickable", "false"),
	    %% android:fullSetProperty("registerButton", "clickable", "true"),
	    cloop();
	update_ok ->
	    android:fullSetProperty("statusBar", "text", "Succesful location update"),
	    cloop();
	not_registered ->
	    android:fullSetProperty("statusBar", "text", "Error: not registered while updating location."),
	    cloop();
	{update_error, Error} ->
	    android:fullSetProperty("statusBar", "text", Error),
	    cloop();
	force_update ->
	    update(),
	    cloop();
	{toofar, {_MaxDistance, _ActualDistance}} ->
	    android:fullSetProperty("statusBar", "text", "The another device is too far away"),
	    cloop();
	{make_ap, {AP_Name, AP_Pwd}} ->
	    {wifiserver,'wifinode@localhost'} ! {make_ap, {AP_Name,AP_Pwd}},
	    android:fullSetProperty("statusBar", "text", "Creating Access Point"),
	    cloop();
	{connect_to, {AP_Name,AP_Passwd,NodeToConnect}} ->
	    {wifiserver,'wifinode@localhost'} ! {connect_to, {AP_Name, AP_Passwd}},
	    android:fullSetProperty("statusBar", "text", "Connecting to an Access Point"),
	    timer:sleep(2000),
	    net_adm:ping(NodeToConnect),
	    cloop();
	missing ->
	    android:fullSetProperty("statusBar", "text", "Error: there's no device with such name."),
	    cloop();
	stop ->
	    stopped;
	{msg, Message} ->
	    android:makeToast(Message);
	_ ->
	    cloop()
    end.
